'''
The script below should return all links from the url https://arstechnica.com. However, there is a bug preventing the
code from running completely. Find the bug and fix it.
'''

import re

import requests

from vendor.bs4 import BeautifulSoup

url = "https://arstechnica.com"
response = requests.get(url)

soup = BeautifulSoup(response.content, "html.parser")

links = []
for link in soup.findAll('a', attrs={'href': re.compile("^http://")}):
    links.append(link.get('href'))

print(links)
