'''
Create the instance attributes fullname and email in the Employee class. Given a person's first and last names:

Form the fullname by simply joining the first and last name together, separated by a space.
Form the email by joining the first and last name together with a . in between, and follow it with @company.com at the end. Make sure the entire email is in lowercase.
'''

emp_1 = Employee(first_name="John", last_name="Smith", bonus=0, salary=50000)
emp_2 = Employee(first_name="Mary", last_name="Sue", bonus=10000, salary=50000)
emp_3 = Employee(first_name="John", last_name="Smith", bonus=10000, salary=90000)

'''
emp_1.fullname ➞ "John Smith"
emp_2.email ➞ "{first_name}.{last_name}@company.com"
emp_3.bonus ➞ 10000
emp_3.bonus_and_salary -> 100000
'''



